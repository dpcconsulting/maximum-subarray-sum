package com.examples;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Kadane_vs_BruteForce(new int[]{-1, -3, 2, 1, -1});
        Kadane_vs_BruteForce(new int[]{-14, -1, 2, 3, -9, 11});
        Kadane_vs_BruteForce(new int[]{-1, 2, 3, -9});
        Kadane_vs_BruteForce(new int[]{-1, 2, 3, -9});
        Kadane_vs_BruteForce(new int[]{2, -1, 2, 3, -9});
        Kadane_vs_BruteForce(new int[]{-1, 2, 3, -9, 11});
        Kadane_vs_BruteForce(new int[]{-2, -1, 1, 2});
        Kadane_vs_BruteForce(new int[]{100, -9, 2, -3, 5});
        Kadane_vs_BruteForce(new int[]{1, 2, 3});
        Kadane_vs_BruteForce(new int[]{-1, -2, -3});

    }

    public static SubArr MaxSubArraySumKadane(int[] intArr) {
//        System.out.println("Calculating maximum subarray sum for " + Arrays.toString(intArr));
        int maxAtCurrentIndex = 0;
        SubArr globalMax = new SubArr(intArr);

        for (int i = 0; i < intArr.length; i++) {
            if (maxAtCurrentIndex > 0) {
                maxAtCurrentIndex += intArr[i];
                globalMax.max_SetIndex_IncLength(maxAtCurrentIndex, i);
            } else {
                maxAtCurrentIndex = intArr[i];
                globalMax.max_SetIndex_SetLenghtToOne(maxAtCurrentIndex, i);
            }
//            System.out.println("c = " + maxAtCurrentIndex + ", g = " + globalMax.getMax() + " " + globalMax);
        }
//        System.out.println("maximal subarray sum: " + globalMax+ "\n\n");
        return globalMax;

    }

    public static SubArr MaxSubArraySumBrute(int[] intArr) {
//        System.out.println("Calculating maximum subarray sum for " + Arrays.toString(intArr));
//        System.out.println("----------------------------------------------");
        // a variable to hold the overall maximum value
        SubArr max = new SubArr(intArr);

        // let's loop through the array
        for (int i = 0; i < intArr.length; i++) {
            // loop through all subarrays that end at position i
            for (int n = 1; n <= i + 1; n++) {
//                printHelper(intArr, i, n);
                // if the sum is bigger than the stored one than replace the stored one
                max.max(EinSum(intArr, i, n), i, n);
            }
//            System.out.println("----------------------------------------------");
        }
//        System.out.println("Kadane's  maximal " + MaxSubArraySumKadane(intArr));
        return max;
    }


    public static String printEin(int[] intArr, int i, int n) {

        Padder p = new Padder(25, ' ');
        // a one line solution
//        return p.pad(Arrays.toString(Arrays.copyOfRange(intArr, i - n + 1, i + 1)));
        if (n == 0) {
            return p.pad("[]");
        }

        String res = "[";
        int k = i - (n - 1);
        for (; k < i; k++) {
            res += intArr[k] + ", ";
        }
        res += intArr[k] + "]";
        return p.pad(res);
    }

    public static int EinSum(int[] intArr, int i, int n) {
        int ret = 0;
        while (n > 0) {
            ret += intArr[i];
            n--;
            i--;
        }
        return ret;
        // a one line solution
//        return Arrays.stream(intArr).skip(i+1-n).limit(n).sum();
    }

    public static void printHelper(int[] intArr, int i, int n) {
        System.out.println("E" + i + n + ": " + printEin(intArr, i, n) + " => " + EinSum(intArr, i, n));
    }

    public static void Kadane_vs_BruteForce(int[] intArr) {
        System.out.println("Calculating maximum subarray sum for " + Arrays.toString(intArr));
        System.out.println("----------------------------------------------");
        System.out.println("Brutforce maximal " + MaxSubArraySumBrute(intArr));
        System.out.println("Kadane's  maximal " + MaxSubArraySumBrute(intArr));
        System.out.println("----------------------------------------------\n");
    }
}
