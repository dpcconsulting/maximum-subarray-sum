package com.examples;

import java.util.Arrays;

public class SubArr {
    private int[] intArr;
    private int max;
    private int i;
    private int n;

    public SubArr(int[] intArr) {
        this.intArr = intArr;
    }

    public void max(int max, int i, int n) {
        if (max > this.max) {
            // store it
            this.max = max;
            this.i = i;
            this.n = n;
        }
    }

    // only in Kadane's algorithm
    public void max_SetIndex_SetLenghtToOne(int max, int i) {
        if (max > this.max) {
            this.max = max;
            this.i = i;
            this.n = 1;
        }
    }

    // only in Kadane's algorithm
    public void max_SetIndex_IncLength(int max, int i) {
        if (max > this.max) {
            this.max = max;
            this.n = this.n + i - this.i;
            this.i = i;
        }
    }

    public int getMax() {
        return max;
    }

    public int getI() {
        return i;
    }

    public int getN() {
        return n;
    }

    public int[] getSubArray() {
        return Arrays.copyOfRange(intArr, i - n + 1, i + 1);
    }

    @Override
    public String toString() {
        return "subarray " + Arrays.toString(Arrays.copyOfRange(intArr, i - n + 1, i + 1))
                + " with sum of " + max;
    }
}
