package com.examples;

public class Padder {
    final int paddingLenght;
    final char paddingChar;
    
    public Padder(int paddingLenght, char paddingChar) {
        this.paddingLenght = paddingLenght;
        this.paddingChar = paddingChar;
    }

    // the trivial solution
    public String pad(String input) {
        if (input == null || paddingLenght <= input.length()) {
            return input;
        } else {
            String pad = "";
            for (int i = 0; i < paddingLenght - input.length(); i++) {
                pad += paddingChar;
            }
            return pad + input;
        }
    }








//    // another solution that uses Array.fill
//    public String pad(String input) {
//        if (input == null || paddingLenght <= input.length()) {
//            return input;
//        } else {
//            char[] c = new char[paddingLenght - input.length()];
//            Arrays.fill(c, paddingChar);
//            return new String(c) + input;
//        }
//    }





//    // use formater to achieve padding, only possible to pad with spaces
//    // so we do a smart replacement of spaces in the original String to
//    // some uncommon character, than back
//    // see Formatter https://docs.oracle.com/javase/8/docs/api/java/util/Formatter.html
//    // note tha using %-17s would align the String left
//    public String pad(String input) {
//        return String.format("%" + paddingLenght + "s", input.replaceAll(" ", "\u2602"))
//                .replaceAll(" ", Character.toString(paddingChar))
//                .replaceAll("\u2602", " ");
//    }

}
